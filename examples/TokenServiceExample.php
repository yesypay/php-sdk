<?php

require 'vendor/autoload.php';

use YesyPay\YesyPayClient;
use YesyPay\YesyPayClientModes;
use YesyPay\TokenService;

define('API_KEY', '222222');

$yesyPayClient = new YesyPayClient(API_KEY, YesyPayClientModes::SANDBOX);
// Enable for local development
$yesyPayClient->setApiHost('localhost:3000');
$yesyPayClient->setApiIsHttps(false);
$tokenService = new TokenService($yesyPayClient);

// -----------------------------------------------------------------------------------
// Tokenization Example
// -----------------------------------------------------------------------------------

// An string can be tokenized
// $token = $tokenService->tokenize('hello');
$importantData = [
    'pan' => '4100123412341234',
    'expirationDate' => '12/22',
    'cardHolder' => 'John Doe',
];
$result = $tokenService->tokenize($importantData);

echo 'Tokenization Result' . PHP_EOL;
print_r($result);

// -----------------------------------------------------------------------------------
// Detokenization Example
// -----------------------------------------------------------------------------------

$token = $result->token;
$result = $tokenService->detokenize($token);

echo 'Detokenization Result' . PHP_EOL;
print_r($result);

// -----------------------------------------------------------------------------------
// Validate Token Example
// -----------------------------------------------------------------------------------

$result = $tokenService->validate($token);

echo 'Validation Result' . PHP_EOL;
print_r($result);

// -----------------------------------------------------------------------------------
// Delete Token Example
// -----------------------------------------------------------------------------------

$result = $tokenService->delete($token);

echo 'Delete Token Result' . PHP_EOL;
print_r($result);
