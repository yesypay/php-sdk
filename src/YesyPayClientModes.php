<?php

namespace YesyPay;

abstract class YesyPayClientModes
{
    const LIVE = 'live';
    const SANDBOX = 'sandbox';
}